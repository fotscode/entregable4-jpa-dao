package com.entrega4.ttps.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.entrega4.ttps.EMF;

public class GenericDao<T> implements CRUD<T> {
    protected Class<T> persistentClass;

    public GenericDao(Class<T> clase) {
        this.persistentClass = clase;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @Override
    public T findById(Long id) {
        EntityManager em = EMF.getEMF().createEntityManager();
        T t = null;
        try {
            t = em.find(persistentClass, id);
        } catch (RuntimeException e) {
            throw e;
        } finally {
            em.close();
        }
        return t;
    }

    @Override
    public List<T> findAll() {
        Query consulta = EMF.getEMF().createEntityManager()
                .createQuery("select e from " + getPersistentClass().getSimpleName() + " e");
        List<T> resultado = (List<T>) consulta.getResultList();
        return resultado;
    }

    @Override
    public T save(T t) {
        EntityManager em = EMF.getEMF().createEntityManager();
        EntityTransaction tx = null;
        try {
            tx = em.getTransaction();
            tx.begin();
            em.persist(t);
            tx.commit();
        } catch (RuntimeException e) {
            if (tx != null && tx.isActive())
                tx.rollback();
            throw e;
        } finally {
            em.close();
        }
        return t;
    }

    @Override
    public T update(T t) {
        EntityManager em = EMF.getEMF().createEntityManager();
        EntityTransaction etx = em.getTransaction();
        etx.begin();
        T entity = em.merge(t);
        etx.commit();
        em.close();
        return entity;
    }

    @Override
    public Boolean delete(T t) {
        EntityManager em = EMF.getEMF().createEntityManager();
        EntityTransaction tx = null;
        try {
            tx = em.getTransaction();
            tx.begin();
            em.remove(em.merge(t));
            tx.commit();
        } catch (RuntimeException e) {
            if (tx != null && tx.isActive())
                tx.rollback();
            throw e;
        } finally {
            em.close();
        }
        return true;
    }
}
