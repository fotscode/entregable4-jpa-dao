package com.entrega4.ttps.dao;

import java.util.List;

public interface CRUD<T> {
    T findById(Long id);
    List<T> findAll();
    T save(T t);
    T update(T t);
    Boolean delete(T t);
}
