package com.entrega4.ttps.dao;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.entrega4.ttps.EMF;
import com.entrega4.ttps.models.Emprendimiento;
import com.entrega4.ttps.models.RedSocial;

public class EmprendimientoDao extends GenericDao<Emprendimiento>{

	public EmprendimientoDao(Class<Emprendimiento> clase) {
		super(clase);
	}

    public Emprendimiento addRedSocial(Emprendimiento e,RedSocial r) {
        EntityManager em = EMF.getEMF().createEntityManager();
        EntityTransaction etx = em.getTransaction();
        etx.begin();
        e.addRedSocial(r);
        Emprendimiento entity = em.merge(e);
        etx.commit();
        em.close();
        return entity;
    }
    public Emprendimiento removeRedSocial(Emprendimiento e, RedSocial r){
        EntityManager em = EMF.getEMF().createEntityManager();
        EntityTransaction etx = null;
        Emprendimiento entity=null;
        try {
            etx = em.getTransaction();
            etx.begin();
            e.removeRedSocial(r);
            entity = em.merge(e); // se rompe, en proceso
            etx.commit();
        }catch(RuntimeException excp){
            if (etx != null && etx.isActive())
                etx.rollback();
            throw excp;
        } finally {
            em.close();
        }
        return entity;
    }

}
