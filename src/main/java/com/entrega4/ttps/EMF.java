package com.entrega4.ttps;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMF {
    private static final EntityManagerFactory emfInstance = Persistence.createEntityManagerFactory("unlp");

    private EMF() {}

    public static EntityManagerFactory getEMF() {
        return emfInstance;
    }
}
