package com.entrega4.ttps.models;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String titulo;
    private String descripcion;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private List<byte[]> fotos;
    @ManyToOne
    private Emprendimiento emprendimiento;

    public void addFoto(byte[] foto) {
        this.fotos.add(foto);
    }
}
