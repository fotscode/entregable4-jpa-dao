package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.AppUser;
import com.entrega4.ttps.models.Role;

public class UserTest {
    private GenericDao<AppUser> userDao = new GenericDao<AppUser>(AppUser.class);
    AppUser user;

    @Before
    public void setUp() throws Exception {
        if (userDao.findAll().size() == 0) {
            user = new AppUser(null, "fixture", "fixture", "fixture", new ArrayList<Role>());
            userDao.save(user);
        }
    }

    @Test
    public void userAddTest() {
        AppUser user = new AppUser(null, "first", "first", "first", new ArrayList<Role>());
        userDao.save(user);
        assertEquals(user.getEmail(), userDao.findById(user.getId()).getEmail());
        userDao.delete(user); // deletes to avoid conflicts with other tests
    }

    @Test
    public void userUpdateTest() {
        user = userDao.findAll().get(0);
        user.setEmail("updated");
        userDao.update(user);
        assertEquals(user.getEmail(), userDao.findById(user.getId()).getEmail());
    }

    @Test
    public void userDeleteTest() {
        user = userDao.findAll().get(0);
        userDao.delete(user);
        assertNull(userDao.findById(user.getId()));
    }

    @Test
    public void userFindAllTest(){  
        AppUser user=new AppUser(null, "find", "find","find", new ArrayList<Role>());
        assertEquals(1, userDao.findAll().size());
        userDao.save(user);
        assertEquals(2, userDao.findAll().size());
        userDao.delete(user); // deletes to avoid conflicts with other tests
    }

}
