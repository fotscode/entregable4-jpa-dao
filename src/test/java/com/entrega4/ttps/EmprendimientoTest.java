package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.EmprendimientoDao;
import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Categoria;
import com.entrega4.ttps.models.Emprendimiento;
import com.entrega4.ttps.models.Manguito;
import com.entrega4.ttps.models.Plan;
import com.entrega4.ttps.models.Post;
import com.entrega4.ttps.models.RedSocial;

public class EmprendimientoTest {
    private EmprendimientoDao emprendimientoDao = new EmprendimientoDao(Emprendimiento.class);
    private GenericDao<RedSocial> redSocialDao = new GenericDao<RedSocial>(RedSocial.class);
    Emprendimiento emprendimiento;

    @Before
    public void setUp() throws Exception {
        if (emprendimientoDao.findAll().size() == 0) { 
            emprendimiento = new Emprendimiento(null, "fixture", "fixture", "fixture", 5, true, true, null, null, new ArrayList<Categoria>(),
                    new ArrayList<Post>(), new ArrayList<Plan>(), new ArrayList<RedSocial>(), new ArrayList<Manguito>());
            emprendimientoDao.save(emprendimiento);
        }
    }

    @Test
    public void emprendimientoAddTest() {
        Emprendimiento emprendimiento = new Emprendimiento(null, "add", "add", "add", 5, true, true, null, null, null,
                null, null, null, null);
        emprendimientoDao.save(emprendimiento);
        assertEquals(emprendimiento.getNombre(), emprendimientoDao.findById(emprendimiento.getId()).getNombre());
        emprendimientoDao.delete(emprendimiento); // deletes to avoid conflicts with other tests
    }

    @Test
    public void emprendimientoUpdateTest() {
        emprendimiento = emprendimientoDao.findAll().get(0);
        emprendimiento.setNombre("updated");
        emprendimientoDao.update(emprendimiento);
        assertEquals(emprendimiento.getNombre(), emprendimientoDao.findById(emprendimiento.getId()).getNombre());
    }

    @Test
    public void emprendimientoDeleteTest() {
        emprendimiento = emprendimientoDao.findAll().get(0);
        emprendimientoDao.delete(emprendimiento);
        assertNull(emprendimientoDao.findById(emprendimiento.getId()));
    }

    @Test
    public void emprendimientoFindAllTest() {
        Emprendimiento emprendimiento = new Emprendimiento(null, "findAll", "findAll", "findAll", 5, true, true, null,
                null, null,
                null, null, null, null);
        assertEquals(1, emprendimientoDao.findAll().size());
        emprendimientoDao.save(emprendimiento);
        assertEquals(2, emprendimientoDao.findAll().size());
        emprendimientoDao.delete(emprendimiento); // deletes to avoid conflicts with other tests
    }

    // esta en proceso
    //@Test
    //public void emprendimientoAddAndRemoveRedSocialTest(){
    //    Emprendimiento emprendimiento = new Emprendimiento(null, "red", "red", "red", 5, true, true, null, null, new ArrayList<Categoria>(), new ArrayList<Post>(), new ArrayList<Plan>(), new ArrayList<RedSocial>(), new ArrayList<Manguito>());
    //    emprendimiento=emprendimientoDao.save(emprendimiento);
    //    RedSocial rs = new RedSocial(null,"red","social");
    //    redSocialDao.save(rs);
    //    emprendimientoDao.addRedSocial(emprendimiento, rs);
    //    for (RedSocial r: emprendimiento.getRedesSociales()){
    //        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    //        System.out.println(r.getNombre());
    //    }
    //    assertEquals(1, emprendimiento.getRedesSociales().size());
    //    //emprendimientoDao.removeRedSocial(emprendimiento, rs);
    //    //for (RedSocial r: emprendimiento.getRedesSociales()){
    //    //    System.out.println("SSSSSSSSSSSSSSSSSS");
    //    //    System.out.println(r.getNombre());
    //    //}
    //    //assertEquals(0, emprendimiento.getRedesSociales().size());
    //    //emprendimientoDao.delete(emprendimiento);
    //}
}
