package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Role;

public class RoleTest {
    private GenericDao<Role> roleDao = new GenericDao<Role>(Role.class);
    Role role;

    @Before
    public void setUp() throws Exception{
        if(roleDao.findAll().size()==0){
            role=new Role(null,"new");
            roleDao.save(role);
        }
    }

    @Test
    public void roleAddTest(){
        Role role=new Role(null, "addtest");
        roleDao.save(role);
        assertEquals(role.getName(), roleDao.findById(role.getId()).getName());
        roleDao.delete(role);
    }

    @Test
    public void pagoPlanUpdateTest(){
        role= roleDao.findAll().get(0);
        role.setName("updated");
        roleDao.update(role);
        assertEquals(role.getName(), roleDao.findById(role.getId()).getName());
    }

    @Test
    public void pagoPlanDeleteTest(){
        role= roleDao.findAll().get(0);
        roleDao.delete(role);
        assertNull(roleDao.findById(role.getId()));
    }

    @Test
    public void pagoPlanFindAllTest(){
        Role role = new Role(null,"findAll");
        assertEquals(1, roleDao.findAll().size());
        roleDao.save(role);
        assertEquals(2, roleDao.findAll().size());
        roleDao.delete(role);
    }
}
