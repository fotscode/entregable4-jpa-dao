package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Plan;

public class PlanTest {
    private GenericDao<Plan> planDao = new GenericDao<Plan>(Plan.class);
    Plan plan;

    @Before
    public void setUp() throws Exception{
        if(planDao.findAll().size()==0){
            plan=new Plan(null,"new","new", 0, null);
            planDao.save(plan);
        }
    }

    @Test
    public void planAddTest(){
        Plan plan=new Plan(null, "addtest", "addtest",0,null);
        planDao.save(plan);
        assertEquals(plan.getTitulo(), planDao.findById(plan.getId()).getTitulo());
        planDao.delete(plan);
    }

    @Test
    public void planUpdateTest(){
        plan = planDao.findAll().get(0);
        plan.setTitulo("UPDATE");
        planDao.update(plan);
        assertEquals(plan.getTitulo(), planDao.findById(plan.getId()).getTitulo());
    }

    @Test
    public void pagoPlanDeleteTest(){
        plan= planDao.findAll().get(0);
        planDao.delete(plan);
        assertNull(planDao.findById(plan.getId()));
    }

    @Test
    public void pagoPlanFindAllTest(){
        Plan pagoPlan = new Plan(null,"findAll", "findAll", 2, null);
        assertEquals(1, planDao.findAll().size());
        planDao.save(pagoPlan);
        assertEquals(2, planDao.findAll().size());
        planDao.delete(pagoPlan);
    }
}
