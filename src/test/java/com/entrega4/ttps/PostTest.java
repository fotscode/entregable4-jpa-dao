package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Post;

public class PostTest {
    private GenericDao<Post> postDao = new GenericDao<Post>(Post.class);
    Post post;

    @Before
    public void setUp() throws Exception{
        if(postDao.findAll().size()==0){
            post=new Post(null,"new","new",null,null);
            postDao.save(post);
        }
    }

    @Test
    public void pagoPlanAddTest(){
        Post post=new Post(null, "addtest", "addtest", null, null);
        postDao.save(post);
        assertEquals(post.getTitulo(), postDao.findById(post.getId()).getTitulo());
        postDao.delete(post);
    }

    @Test
    public void pagoPlanUpdateTest(){
        post= postDao.findAll().get(0);
        post.setTitulo("updated");
        postDao.update(post);
        assertEquals(post.getTitulo(), postDao.findById(post.getId()).getTitulo());
    }

    @Test
    public void pagoPlanDeleteTest(){
        post= postDao.findAll().get(0);
        postDao.delete(post);
        assertNull(postDao.findById(post.getId()));
    }

    @Test
    public void pagoPlanFindAllTest(){
        Post post = new Post(null,"findAll", "findAll", null, null);
        assertEquals(1, postDao.findAll().size());
        postDao.save(post);
        assertEquals(2, postDao.findAll().size());
        postDao.delete(post);
    }
}
