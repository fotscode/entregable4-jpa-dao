package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.RedSocial;

public class RedSocialTest {
    private GenericDao<RedSocial> redSocialDao = new GenericDao<RedSocial>(RedSocial.class);
    RedSocial redSocial;

    @Before
    public void setUp() throws Exception {
        if (redSocialDao.findAll().size() == 0) {
            redSocial = new RedSocial(null,"fixture","fixture");
            redSocialDao.save(redSocial);
        }
    }

    @Test
    public void redSocialAddTest() {
        RedSocial redSocial = new RedSocial(null, "first", "first");
        redSocialDao.save(redSocial);
        assertEquals(redSocial.getNombre(), redSocialDao.findById(redSocial.getId()).getNombre());
        redSocialDao.delete(redSocial); // deletes to avoid conflicts with other tests)
    }

    @Test
    public void redSocialUpdateTest() {
        RedSocial rsAdd = new RedSocial(null, "update", "update");
        redSocialDao.save(rsAdd);
        rsAdd.setNombre("changed");
        rsAdd =redSocialDao.update(rsAdd);
        assertEquals("changed", redSocialDao.findById(rsAdd.getId()).getNombre());
        redSocialDao.delete(rsAdd);
    }

    @Test
    public void redSocialDeleteTest() {
        RedSocial rsDel = new RedSocial(null, "delete", "delete");
        redSocialDao.save(rsDel);
        redSocialDao.delete(rsDel);
        assertNull(redSocialDao.findById(rsDel.getId()));
    }

    @Test
    public void redSocialFindAllTest(){  
        RedSocial redSocial=new RedSocial(null, "find", "find");
        assertEquals(1, redSocialDao.findAll().size());
        redSocialDao.save(redSocial);
        assertEquals(2, redSocialDao.findAll().size());
        redSocialDao.delete(redSocial); // deletes to avoid conflicts with other tests
    }
}
