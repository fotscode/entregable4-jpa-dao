package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.PagoPlan;

public class PagoPlanTest {
    private GenericDao<PagoPlan> pagoPlanDao = new GenericDao<PagoPlan>(PagoPlan.class);
    PagoPlan pagoPlan;

    @Before
    public void setUp() throws Exception{
        if(pagoPlanDao.findAll().size()==0){
            pagoPlan=new PagoPlan(null,"Pepe Gonzalez","pepegonza@gmail.com",null,3250.5,"",null);
            pagoPlanDao.save(pagoPlan);
        }
    }

    @Test
    public void pagoPlanAddTest(){
        PagoPlan pagoPlan=new PagoPlan(null, "new", "new", null, 0, "new", null);
        pagoPlanDao.save(pagoPlan);
        assertEquals(pagoPlan.getNombrePersona(), pagoPlanDao.findById(pagoPlan.getId()).getNombrePersona());
        pagoPlanDao.delete(pagoPlan);
    }

    @Test
    public void pagoPlanUpdateTest(){
        pagoPlan= pagoPlanDao.findAll().get(0);
        pagoPlan.setNombrePersona("updated");
        pagoPlanDao.update(pagoPlan);
        assertEquals(pagoPlan.getNombrePersona(), pagoPlanDao.findById(pagoPlan.getId()).getNombrePersona());
    }

    @Test
    public void pagoPlanDeleteTest(){
        pagoPlan= pagoPlanDao.findAll().get(0);
        pagoPlanDao.delete(pagoPlan);
        assertNull(pagoPlanDao.findById(pagoPlan.getId()));
    }

    @Test
    public void pagoPlanFindAllTest(){
        PagoPlan pagoPlan = new PagoPlan(null,"findAll", "findAll", null, 2, "findAll", null);
        assertEquals(1, pagoPlanDao.findAll().size());
        pagoPlanDao.save(pagoPlan);
        assertEquals(2, pagoPlanDao.findAll().size());
        pagoPlanDao.delete(pagoPlan);
    }
}
