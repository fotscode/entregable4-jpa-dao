package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Manguito;

public class ManguitoTest {
    private GenericDao<Manguito> manguitoDao = new GenericDao<Manguito>(Manguito.class);
    Manguito manguito;

    @Before
    public void setUp() throws Exception {
        if (manguitoDao.findAll().size() == 0) {
            manguito = new Manguito(null,"fixture",null,5,5,"fixture",null);
            manguitoDao.save(manguito);
        }
    }

    @Test
    public void manguitoAddTest() {
        Manguito manguito = new Manguito(null,"find",null,5,5,"find",null);
        manguitoDao.save(manguito);
        assertEquals(manguito.getNombrePersona(), manguitoDao.findById(manguito.getId()).getNombrePersona());
        manguitoDao.delete(manguito); // deletes to avoid conflicts with other tests)
    }

    @Test
    public void manguitoUpdateTest() {
        manguito =manguitoDao.findAll().get(0);
        manguito.setNombrePersona("updated");
        manguitoDao.update(manguito);
        assertEquals(manguito.getNombrePersona(), manguitoDao.findById(manguito.getId()).getNombrePersona());
    }

    @Test
    public void manguitoDeleteTest() {
        manguito =manguitoDao.findAll().get(0);
        manguitoDao.delete(manguito);
        assertNull(manguitoDao.findById(manguito.getId()));
    }

    @Test
    public void manguitoFindAllTest(){  
        Manguito manguito = new Manguito(null,"first",null,5,5,"first",null);
        assertEquals(1, manguitoDao.findAll().size());
        manguitoDao.save(manguito);
        assertEquals(2, manguitoDao.findAll().size());
        manguitoDao.delete(manguito); // deletes to avoid conflicts with other tests
    }
}
