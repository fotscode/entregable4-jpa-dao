package com.entrega4.ttps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.entrega4.ttps.dao.GenericDao;
import com.entrega4.ttps.models.Categoria;

public class CategoriaTest {
    private GenericDao<Categoria> categoriaDao = new GenericDao<Categoria>(Categoria.class);
    Categoria categoria;

    @Before
    public void setUp() throws Exception {
        if (categoriaDao.findAll().size() == 0) {
            categoria = new Categoria(null,"fixture","fixture");
            categoriaDao.save(categoria);
        }
    }

    @Test
    public void categoriaAddTest() {
        Categoria categoria = new Categoria(null, "first", "first");
        categoriaDao.save(categoria);
        assertEquals(categoria.getNombre(), categoriaDao.findById(categoria.getId()).getNombre());
        categoriaDao.delete(categoria); // deletes to avoid conflicts with other tests)
    }

    @Test
    public void categoriaUpdateTest() {
        categoria =categoriaDao.findAll().get(0);
        categoria.setNombre("updated");
        categoriaDao.update(categoria);
        assertEquals(categoria.getNombre(), categoriaDao.findById(categoria.getId()).getNombre());
    }

    @Test
    public void categoriaDeleteTest() {
        categoria =categoriaDao.findAll().get(0);
        categoriaDao.delete(categoria);
        assertNull(categoriaDao.findById(categoria.getId()));
    }

    @Test
    public void categoriaFindAllTest(){  
        Categoria categoria=new Categoria(null, "find", "find");
        assertEquals(1, categoriaDao.findAll().size());
        categoriaDao.save(categoria);
        assertEquals(2, categoriaDao.findAll().size());
        categoriaDao.delete(categoria); // deletes to avoid conflicts with other tests
    }
}
