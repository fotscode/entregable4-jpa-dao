# Dependencias
- mariadb / mysql
- maven
- open-jdk 18 (o configurar el pom.xml)
- git

# Pasos a realizar
- Clonar el repositorio "git clone https://gitlab.com/fotscode/entregable4-jpa-dao"
- Cambiar el archivo ubicado en src/main/resources/META-INF/persistence.xml para que coincida con la configuracion local (bd, usuario, pwd). Aca si estas usando mysql tenes que cambiar el driver.
- Por ultimo realizar "mvn clean install"

# Tests
- Usando "mvn test"
